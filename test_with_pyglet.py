# © 2022 Castix <castix@autistici.org> 16BA47F0EC01AF6F
# pip install JACK-Client numpy pyglet
# apt install sox wget
from ctypes import ArgumentError
from jack import Client
from datetime import datetime
from time import time
from itertools import chain
from subprocess import Popen, DEVNULL
from sys import stderr
import threading
import math
import pyglet


RMM = 'https://s.streampunk.cc/_stream/merdamalata.ogg'
_rmm = None


def start_radio():
    # play from sox
    # a subprocess is needed because when running in a thread, the stream gets interrupted after about a minute
    global _rmm
    _rmm = Popen(['play', RMM], stdout=DEVNULL, stderr=DEVNULL)
    _rmm.wait()


_meters = {}
_peaks = {}

window = pyglet.window.Window(width=100, height=100)
image = pyglet.resource.image('assets/bar.jpg')
max_h = 70
bar_width = 10
label_capture = pyglet.text.Label(
    'Capture',
    font_name='Times New Roman',
    font_size=10, x=10, y=85
)
label_radio = pyglet.text.Label(
    'Radio',
    font_name='Times New Roman',
    font_size=10, x=60, y=85
)
batch_meters = pyglet.graphics.Batch()
batch_peaks = pyglet.graphics.Batch()


def get_h(_meter):
    return abs(max_h + to_db(_meter))


@window.event
def on_draw():
    window.clear()
    image.blit(0, 0)
    image.blit(50, 0)
    label_capture.draw()
    label_radio.draw()
    for index, meter in enumerate(_meters.items()):
        cords = tuple(chain.from_iterable([(
            (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + j, 10,  # start
            (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + j, 10 + int(get_h(meter[1]['value'])),  # end
        ) for j in range(1, 11)]))
        howmany_lines = int(len(cords) / 4)
        vertexes = batch_meters.add(
            howmany_lines * 2,
            pyglet.gl.GL_LINES,
            None,
            ('v2i', cords),
            ('c3B', howmany_lines * to_color(to_db(meter[1]['value']))),
        )
        batch_meters.draw()
        vertexes.delete()
    for index, peak in enumerate(_peaks.items()):
        cords = [
            (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) - 2, 10 + int(get_h(peak[1]['value'])),  # start
            (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + 12, 10 + int(get_h(peak[1]['value'])),  # end
        ]
        howmany_lines = int(len(cords) / 4)
        vertexes = batch_peaks.add(
            howmany_lines * 2,
            pyglet.gl.GL_LINES,
            None,
            ('v2i', cords),
            ('c3B', howmany_lines * ((255,) * 6))
        )
        batch_peaks.draw()
        vertexes.delete()


def draw_bar(_):
    # pass is enough to trigger an event and call on_draw
    pass


pyglet.clock.schedule_interval(draw_bar, 1/30)

client = Client('silmon')


def to_db(x: float) -> float:
    try:
        return 20 * math.log10(abs(x))
    except ValueError as ex:
        return -70  # should be 0 but gui shows -70...


def to_color(db: float) -> tuple:
    if db < -2:
        color = (0, 255, 0, 0, 255, 0)
    if db < -5:
        color = (0, 255, 0, 255, 255, 0)
    elif db < -10:
        color = (0, 255, 0, 255, 128, 0)
    else:
        color = (0, 255, 0, 255, 0, 0)
    return color


@client.set_process_callback
def process(frames):
    assert frames == client.blocksize
    thereshold = 0.3
    peak_timeout = 1
    notification_again_after = 10
    for p in client.inports:
        n = p.shortname
        now = time()
        _meters.setdefault(n, {'value': -1, 'timestamp': 0})
        _peaks.setdefault(n, {'value': -1, 'timestamp': now, 'notified': now})
        for x in p.get_array():
            if n not in _meters or _meters[n]['value'] < x:
                _meters[n]['value'] = x
                _meters[n]['timestamp'] = now
            if now - _meters[n]['timestamp'] > 1/30:  # reset 30 times per second
                _meters[n]['value'] = -1
                _meters[n]['timestamp'] = now
            if (
                n not in _peaks
                or _peaks[n]['value'] < x
                or now - _peaks[n]['timestamp'] > peak_timeout
            ):
                _peaks[n]['value'] = x
                _peaks[n]['timestamp'] = now

        # todo: for each client make configurable:
        # - startup delay
        # - silence thereshold
        # - warn if silence persists more than timeout
        if (
            now - _peaks[n]['timestamp'] > peak_timeout
            and _peaks[n]['value'] < thereshold
            and now - _peaks[n]['notified'] > notification_again_after
        ):
            print(f'{p.name} is silent!!!', file=stderr)
            _peaks[n]['notified'] = now


@client.set_port_registration_callback
def port_reg(*a, **kw):
    print('reg', a, kw)
    if a[0].name.startswith('SoX') and a[1]:
        if a[0].shortname == 'output_FL':
            client.connect(a[0], client.inports[2])
        if a[0].shortname == 'output_FR':
            client.connect(a[0], client.inports[3])


@client.set_shutdown_callback
def shutdown(status, reason):
    print('JACK shutdown!')
    print('status:', status)
    print('reason:', reason)
    event.set()


event = threading.Event()

client.inports.register('capture_L')
client.inports.register('capture_R')
client.inports.register('rmm_L')
client.inports.register('rmm_R')


with client:
    # When entering this with-statement, client.activate() is called.
    # This tells the JACK server that we are ready to roll.
    # Our process() callback will start running now.

    # Connect the ports.  You can't do this before the client is activated,
    # because we can't make connections to clients that aren't running.
    # Note the confusing (but necessary) orientation of the driver backend
    # ports: playback ports are "input" to the backend, and capture ports
    # are "output" from it.

    capture = client.get_ports(is_physical=True, is_output=True)

    client.connect(capture[0], client.inports[0])
    client.connect(capture[1], client.inports[1])

    gui = threading.Thread(target=pyglet.app.run, name='gui')
    try:
        gui.start()
    except ArgumentError:
        pass  # this happens after KeyboardInterrupt, before closing the window
    rmm = threading.Thread(target=start_radio, name='radio')
    rmm.start()
    try:
        print('Press Ctrl+C to stop')
        event.wait()
    except KeyboardInterrupt:
        print('CIAONE')
        try:
            window.close()
            gui.join()
        except KeyboardInterrupt:
            print('gui interruption interrupted by user')
        try:
            _rmm.terminate()  # terminate the subprocess
            rmm.join()
        except KeyboardInterrupt:
            print('radio interruption interrupted by user')


