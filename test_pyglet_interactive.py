from threading import Thread
import pyglet
#pyglet.options['debug_gl'] = True
#pyglet.options['debug_gl_trace'] = True
#pyglet.options['debug_gl_trace_args'] = True

# You do need to store things you want to interactively change
# and for thread-safety you should use integers as kict keys...
state = {
    "line_position": [0, 0, 100, 100],
    "line_color": [255] * 8,  # RGBA * 2 vertexes
}

def debug():
    breakpoint()
    # (Pdb++) state['line_color'] = [0.5, 1, 0.5, 1,     1, 0, 0, 1,]
    # You'd better use state.update()

window = pyglet.window.Window(200, 200)
program = pyglet.shapes.get_default_shader()
batch = pyglet.graphics.Batch()
line_vertexes = program.vertex_list(2, pyglet.gl.GL_LINES, batch)


@window.event()
def on_key_press(*a, **kw):
    print(chr(a[0]))
    '''
    # Horrible idea, ghosting
    if a[0] == ord('c'):
        print('clear')
        window.clear()
    '''

@window.event()
def on_draw():
    window.clear()
    batch.draw()
    # maybe also delete them when not needed anymore
    # line_vertexes.delete()


def update(dt):
    line_vertexes.position = state['line_position']
    line_vertexes.color = state['line_color']

pyglet.clock.schedule_interval(update, 1/120)

t = Thread(target=debug)
t.start()
pyglet.app.run()
t.join()
