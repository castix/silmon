# © 2022, 2025 Castix <castix@autistici.org>
# pip install JACK-Client numpy pyglet==2.1
# apt install sox wget

# diff of pyglet 1.5-->2.1 migration:
# - we can't start pyglet app in a thread anymore.
#   https://github.com/pyglet/pyglet/issues/793 (though, that's not what we're experiencing)
# - we are still happy with the default shading, but can't use v2i c3B legacy opengl.
#   useful: https://github.com/pyglet/pyglet/issues/715
# - colors are now RGBA float instead of a RGB bytes
# - the peak bar becomes red when at the top (that's almost our only user facing diff)
# - it felt dumb to use a thread just to wait a subprocess
# - jack_client in the same thread, in our case seems nicer without context manager
# - playing the radio currently disabled, as my alsa->jack bridge isn't working...
from jack import Client, JackErrorCode, JackOpenError
from datetime import datetime
from time import time
from itertools import chain
from subprocess import Popen, DEVNULL
from sys import stderr
import math
import pyglet
#pyglet.options['debug_gl'] = True
#pyglet.options['debug_gl_trace'] = True
#pyglet.options['debug_gl_trace_args'] = True


RMM = 'https://s.streampunk.cc/_stream/merdamalata.ogg'
_rmm = None


def start_radio():
    # play from sox
    # a subprocess is needed because when running in a thread, the stream gets interrupted after about a minute
    global _rmm
    _rmm = Popen(['play', RMM], stdout=DEVNULL, stderr=DEVNULL)


_meters = {}
_peaks = {}

window = pyglet.window.Window(width=100, height=100)
image = pyglet.resource.image('assets/bar.jpg')
max_h = 70
bar_width = 10
label_capture = pyglet.text.Label(
    'Capture',
    font_name='Times New Roman',
    font_size=10, x=10, y=85
)
label_radio = pyglet.text.Label(
    'Radio',
    font_name='Times New Roman',
    font_size=10, x=60, y=85
)
program = pyglet.shapes.get_default_shader()
batch_meters = pyglet.graphics.Batch()
batch_peaks = pyglet.graphics.Batch()


def get_h(_meter):
    return abs(max_h + to_db(_meter))


@window.event
def on_draw():
    try:
        window.clear()
        image.blit(0, 0)
        image.blit(50, 0)
        label_capture.draw()
        label_radio.draw()
        try:
            # you need to reset this batch...
            batch_meters = pyglet.graphics.Batch()
            for index, meter in enumerate(_meters.items()):
                coords = tuple(chain.from_iterable([(
                    (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + j, 10,  # start
                    (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + j, 10 + int(get_h(meter[1]['value'])),  # end
                ) for j in range(1, 11)]))
                howmany_lines = int(len(coords) / 4)
                # never forget to save a reference to the vertexes
                vertexes = program.vertex_list(
                    howmany_lines * 2,
                    pyglet.gl.GL_LINES,
                    batch_meters,
                )
                vertexes.position = coords
                vertexes.color = howmany_lines * to_color(to_db(meter[1]['value']))

                batch_meters.draw()
            for index, peak in enumerate(_peaks.items()):
                current_h = 10 + int(get_h(peak[1]['value']))
                coords = [
                    (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) - 2, current_h,   # start
                    (10 if index % 2 == 0 else 30) + (50 * int(index / 2)) + 12, current_h,  # end
                ]
                howmany_lines = int(len(coords) / 4)
                color = (255, 0, 0) if current_h == 10 + max_h else (255,) * 3
                # you need a reference to the line
                line = pyglet.shapes.Line(*coords, color=color, batch=batch_peaks)
                batch_peaks.draw()
        except RuntimeError as exc:
            print('RuntimeError', exc)
    except KeyboardInterrupt:
        print('on_draw interrupted')
        raise  # do not raise from, we are hiding a useless traceback


def draw_bar(_):
    # pass is enough to trigger an event and call on_draw
    pass


pyglet.clock.schedule_interval(draw_bar, 1/30)

try:
    client = Client('silmon', use_exact_name=True, no_start_server=True)
except JackOpenError:
    raise BaseException('I expected jack to be already started, do you?')


def to_db(x: float) -> float:
    try:
        return 20 * math.log10(abs(x))
    except ValueError as ex:
        return -70  # should be 0 but gui shows -70...


def to_color(db: float) -> tuple:
    if db < -2:
        color = (
            0, 1, 0, 1,  # color for the first vertex of the line
            0, 1, 0, 1,  # color for the last vertex of the line
        )
    if db < -5:  # NOTE: no elif, no multiple return
        color = (
            0, 1, 0, 1,
            1, 1, 0, 1,
        )
    elif db < -10:
        color = (
            0, 1, 0, 1,
            1, .5, 0, 1,
        )
    else:
        color = (
            .5, 1, 0, 1,
            1, 0, 0, 1,
        )
    return color


@client.set_process_callback
def process(frames):
    assert frames == client.blocksize
    thereshold = 0.3
    peak_timeout = 1
    notification_again_after = 10
    for p in client.inports:
        n = p.shortname
        now = time()
        _meters.setdefault(n, {'value': -1, 'timestamp': 0})
        _peaks.setdefault(n, {'value': -1, 'timestamp': now, 'notified': now})
        for x in p.get_array():
            if _meters[n]['value'] < x:
                _meters[n]['value'] = x
                _meters[n]['timestamp'] = now
            if now - _meters[n]['timestamp'] > 1/30:  # reset 30 times per second
                _meters[n]['value'] = -1
                _meters[n]['timestamp'] = now
            if (
                _peaks[n]['value'] < x
                or now - _peaks[n]['timestamp'] > peak_timeout
            ):
                _peaks[n]['value'] = x
                _peaks[n]['timestamp'] = now

        # todo: for each client make configurable:
        # - startup delay
        # - silence thereshold
        # - warn if silence persists more than timeout
        if (
            now - _peaks[n]['timestamp'] > peak_timeout
            and _peaks[n]['value'] < thereshold
            and now - _peaks[n]['notified'] > notification_again_after
        ):
            print(f'{p.name} is silent!!!', file=stderr)
            _peaks[n]['notified'] = now


@client.set_port_registration_callback
def port_reg(*a, **kw):
    print('registered new port', a[0].shortname())
    if a[0].name.startswith('SoX') and a[1]:
        if a[0].shortname == 'output_FL':
            client.connect(a[0], client.inports[2])
        if a[0].shortname == 'output_FR':
            client.connect(a[0], client.inports[3])


@client.set_shutdown_callback
def shutdown(status, reason):
    print('JACK asks our client to shutdown!')
    print('status:', status)
    print('reason:', reason)
    client.close()
    # you might also like to be aware of https://github.com/jackaudio/jack2/issues/395


@client.set_xrun_callback
def xrun(delayed_usecs: float) -> None:
    print(f'XRUN: {delayed_usecs}μs')
    # UHm,,, you can expect some xrun when pyglet is about to start it's first frame
    # otherwise, check your jackd settings such as buffer size


client.inports.register('capture_L')
client.inports.register('capture_R')
client.inports.register('rmm_L')
client.inports.register('rmm_R')

print('Activating jack client')
client.activate()  # then
# Connect the ports.  You can't do this before the client is activated,
# because we can't make connections to clients that aren't running.
# Note the confusing (but necessary) orientation of the driver backend
# ports: playback ports are "input" to the backend, and capture ports
# are "output" from it.

capture = client.get_ports(is_physical=True, is_output=True)

client.connect(capture[0], client.inports[0])
client.connect(capture[1], client.inports[1])


#start_radio()
try:
    print('Starting the GUI')
    pyglet.app.run()
except KeyboardInterrupt:
    print('okay, exiting')

try:
    window.close()
except KeyboardInterrupt:
    print('closing the window was interrupted by user')
try:
    #_rmm.terminate()  # terminate the subprocess
    ...
except KeyboardInterrupt:
    print('radio interruption interrupted by user')


for port in client.inports:
    try:
        port.disconnect()
    except JackErrorCode:
        ...  # jackd was stopped
client.deactivate()
client.close()
