# © 2022 Castix <castix@autistici.org> 16BA47F0EC01AF6F
# pip install JACK-Client numpy
from jack import Client
from datetime import datetime
from time import time
import threading
import math

client = Client('nosilence', use_exact_name=True, no_start_server=True)
_peak = 0
_peak_timestamp = time()

FIVE_MINUTES = 60 * 5

def to_db(x: float) -> float:
    return 20 * math.log10(abs(x))


@client.set_process_callback
def process(frames):
    global _peak, _peak_timestamp
    assert frames == client.blocksize
    for x in client.inports[0].get_array():
        if _peak < x:
          _peak = x
          _peak_timestamp = time()
          print('%s PEAK: %.3f' % (datetime.utcfromtimestamp(_peak_timestamp), to_db(_peak)))
        if time() - _peak_timestamp > FIVE_MINUTES:
            print('%s resetting peak' % time())
            _peak = 0
            _peak_timestamp = time()


@client.set_port_registration_callback
def port_reg(*a, **kw):
    print(a, kw)


@client.set_shutdown_callback
def shutdown(status, reason):
    print('JACK shutdown!')
    print('status:', status)
    print('reason:', reason)
    event.set()


event = threading.Event()


for number in (1, 2):
    client.inports.register(f'input_{number}')

with client:
    # When entering this with-statement, client.activate() is called.
    # This tells the JACK server that we are ready to roll.
    # Our process() callback will start running now.

    # Connect the ports.  You can't do this before the client is activated,
    # because we can't make connections to clients that aren't running.
    # Note the confusing (but necessary) orientation of the driver backend
    # ports: playback ports are "input" to the backend, and capture ports
    # are "output" from it.

    capture = client.get_ports(is_physical=True, is_output=True)
    if not capture:
        raise BaseException('No physical capture ports')

    for src, dest in zip(capture, client.inports):
        client.connect(src, dest)

    playback = client.get_ports(is_physical=True, is_input=True)
    if not playback:
        raise BaseException('No physical playback ports')

    for src, dest in zip(client.outports, playback):
        client.connect(src, dest)

    print('Press Ctrl+C to stop')
    try:
        event.wait()
    except KeyboardInterrupt:
        print('\nInterrupted by user')
